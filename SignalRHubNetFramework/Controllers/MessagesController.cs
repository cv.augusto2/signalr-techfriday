﻿using Microsoft.AspNet.SignalR;
using SignalRHubFramework.Models;
using SignalRHubNetFramework.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SignalRHubNetFramework.Controllers
{
    public class MessagesController : ApiController
    {
        private IHubContext _hubContext;

        public MessagesController()
        {
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<NotifyHub>();            
        }

        [HttpPost]
        public string Post([FromBody]Message msg)
        {
            string retMessage = string.Empty;

            try
            {
                _hubContext.Clients.All.BroadcastMessage(msg.Type, msg.Payload);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }

            return retMessage;
        }
    }
}
