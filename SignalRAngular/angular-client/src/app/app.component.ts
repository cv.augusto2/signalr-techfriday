import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from "@aspnet/signalr"; // .NET Core
import { Message } from "primeng/api";
import { ToastrService } from 'ngx-toastr';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private hubConnection: any;
  private hubConnectionCore: HubConnection;
  private proxyHub: any;
  public msgs: Message[] = [];

  constructor(
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    // .NET Core
    this.hubConnectionCore = new HubConnectionBuilder().withUrl('http://localhost:16437/signalr').build();

    this.hubConnectionCore
      .start()
      .then(() => console.log('Conexão estabelecida com Endpoint'))
      .catch(err => console.log('Erro ao estabelecer conexão com o Endpoint'));

    this.hubConnectionCore.on('BroadcastMessage', (type: string, payload: string) => {
      this.msgs.push({ severity: type, summary: payload });
    });

    // .NET Framework
    // this.hubConnection = $.hubConnection("http://localhost:16438/signalr");

    // this.hubConnection
    //   .start()
    //   .then(() => console.log('Conexão estabelecida com Endpoint'))
    //   .catch(err => console.log('Erro ao estabelecer conexão com o Endpoint'));

    // this.proxyHub = this.hubConnection.createHubProxy("notify");
    // this.proxyHub.on('BroadcastMessage', (type: string, payload: string) => {
    //   // this.msgs.push({ severity: 'Success', summary: payload });
    //   this.showNotification(type, payload);
    //   console.log('Type: ' + type, 'Summary: ' + payload);
    // });

  }

  public showNotification(pSeverity: string, pSummary: string):void {
    this.msgs.push({ severity: pSeverity, summary: pSummary });
    this.toastr.success(pSummary, "Resposta SignalR");
  }
}
